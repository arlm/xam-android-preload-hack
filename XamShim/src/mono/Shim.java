package mono;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Shim
        extends android.content.ContentProvider
{
    public Shim ()
    {
    }

    @Override
    public boolean onCreate ()
    {
        return true;
    }

    @Override
    public void attachInfo (android.content.Context context, android.content.pm.ProviderInfo info) {
        LoadApplication (context, context.getApplicationInfo ().dataDir, new String[]{context.getApplicationInfo ().sourceDir});
        super.attachInfo (context, info);
    }
    public static void LoadApplication(Context context, String runtimeDataDir, String[] apks) {
        File data = new File(context.getApplicationInfo ().dataDir);
        File libs = new File(data, "lib");

        Log.e("mono-shim", "code path " + context.getPackageCodePath());


        if(!context.getPackageCodePath().startsWith("/system"))
            return;

        Log.e("mono-shim", "attempting alternate load");

        File apk = new File(context.getPackageCodePath());
        String apk_name = apk.getName();
        String name_only = apk_name.substring(0, apk_name.length() - 4);

        if(new File("/system/lib/libmonosgen-2.0.so").exists())
            libs = new File("/system/lib");
        else if(new File("/system/lib/" + name_only + "/libmonosgen-2.0.so").exists())
            libs = new File("/system/lib/" + name_only);
        else if(new File("/system/lib/" + name_only + "/x86/libmonosgen-2.0.so").exists())
            libs = new File("/system/lib/" + name_only + "/x86");
        else if(new File("/system/lib/" + name_only + "/armeabi-v7a/libmonosgen-2.0.so").exists())
            libs = new File("/system/lib/" + name_only + "/armeabi-v7a");

        synchronized (MonoPackageManager.lock) {
            if (!MonoPackageManager.initialized) {
                System.loadLibrary("monodroid");
                Locale locale       = Locale.getDefault ();
                String language     = locale.getLanguage () + "-" + locale.getCountry ();
                String filesDir     = context.getFilesDir ().getAbsolutePath ();
                String cacheDir     = context.getCacheDir ().getAbsolutePath ();
                String dataDir      = context.getApplicationInfo ().dataDir + "/lib";
                ClassLoader loader  = context.getClassLoader ();

                mono.android.Runtime.init (
                        language,
                        apks,
                        runtimeDataDir,
                        new String[]{
                                filesDir,
                                cacheDir,
                                libs.getAbsolutePath(),
                        },
                        loader,
                        new java.io.File (
                                android.os.Environment.getExternalStorageDirectory (),
                                "Android/data/" + context.getPackageName () + "/files/.__override__").getAbsolutePath (),
                        MonoPackageManager_Resources.Assemblies,
                        context.getPackageName ());
                MonoPackageManager.initialized = true;
            }
        }
    }


    @Override
    public android.database.Cursor query (android.net.Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        throw new RuntimeException ("This operation is not supported.");
    }

    @Override
    public String getType (android.net.Uri uri)
    {
        throw new RuntimeException ("This operation is not supported.");
    }

    @Override
    public android.net.Uri insert (android.net.Uri uri, android.content.ContentValues initialValues)
    {
        throw new RuntimeException ("This operation is not supported.");
    }

    @Override
    public int delete (android.net.Uri uri, String where, String[] whereArgs)
    {
        throw new RuntimeException ("This operation is not supported.");
    }

    @Override
    public int update (android.net.Uri uri, android.content.ContentValues values, String where, String[] whereArgs)
    {
        throw new RuntimeException ("This operation is not supported.");
    }
}

